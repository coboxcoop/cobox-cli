# cobox-cli

A command line interface for cobox. Start the app, then create spaces, mount their file systems, swarm on the peer-to-peer network to replicate the latest changes, and setup your own backup 'blind replicators'.

## Install

``server `
npm i -g cobox-cli

cobox server start
```

This will launch a process which you can see by navigating in a browser to

`http://localhost:3000`

If you already have something running on port 3000 then you can run

```
cobox start --port 1234
```

You can also specify a custom mount directory for your spaces' file systems.

```
cobox start --mount ./mnt
```

Great, now you have a server running. In another terminal you can run (you can use any name you like)

```
node cli spaces create --name my-new-space
```

If you have been given an address and an encryption key for a space you want to join, you can create the space by adding the flags `--address` / `-A`, or `--encryption-key` or `-k`
```
node cli spaces create -n my-new-space -A b74717c149390c3e8e939f6e94169751d9ade8e2e21e0255ee593fdccb504603 -k 0e90565c93f742019f0956ce7da1c952df343dde4a2c48254fbbb9cbcd7d22be
```

```
node cli spaces create --name my-new-space
```

If you have been given an address to become a replicator for a friend or organisation, say... `the-chicken-coop`, you can set this up with the following command

```
node cli replicators join -n the-chicken-coop -A b74717c149390c3e8e939f6e94169751d9ade8e2e21e0255ee593fdccb504603
```

You can see the list of other commands implemented here

```
cobox help
cobox spaces help
cobox replicators help
```

const chalk = require('chalk')
const readline = require('readline')

function printSpaces (spaces) {
  spaces = Array.isArray(spaces) ? spaces : [spaces]
  console.log("-- Name --".padEnd(40), "-- Address --".padEnd(64))
  for (const space of spaces) {
    const name = space.name || ''
    console.log(
      name.padEnd(40),
      chalk.green(space.address),
    )
  }
  console.log()
}

function getInput (message, cb) {
  const rl = readline.createInterface({ input: process.stdin, output: process.stdout })
  rl.question(message, (answer) => {
    if (!answer) return getInput(message, cb)
    cb(answer)
    rl.close()
  })
}

function onSuccess (msgs) {
  msgs = Array.isArray(msgs) ? msgs : [msgs]
  for (const msg of msgs) console.log(msg, '\n')
}

function onError (errs) {
  errs = Array.isArray(errs) ? errs : [errs]
  for (const err of errs) console.error(chalk.red(err), '\n')
}

function getName (name) {
  var prompt = 'provide a name for your space...\n'
  return new Promise((resolve, reject) => {
    return next(null, name)

    function next (err, name) {
      if (err) return reject(err)
      if (!name) getInput(prompt, next)
      else return resolve(name)
    }
  })
}

function printErrors (error) {
  var response = error.response
  if (!response) return onError(error)
  var errors = response.data.errors
  return onError(errors.map((err) => err.msg))
}

function readableBytes (bytes) {
  if (bytes < 1) return 0 + ' B'
  const i = Math.floor(Math.log(bytes) / Math.log(1024))
  const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  return (bytes / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i]
}

function printBanner () {
console.log(`
                [0;34m888[0m                      
                [0;34m888[0m                      
                [0;37m888[0m                      
 [0;34m.d8888b[0m [0;37m.d88b.[0m [0;37m88888b.[0m  [0;1;30;90m.d88b.[0m [0;1;30;90m888[0m  [0;1;30;90m888[0m 
[0;37md88P"[0m   [0;37md88""88b[0;1;30;90m888[0m [0;1;30;90m"88bd88""88b[0;1;34;94m\`Y8bd8P'[0m 
[0;37m888[0m     [0;1;30;90m888[0m  [0;1;30;90m888888[0m  [0;1;30;90m888[0;1;34;94m888[0m  [0;1;34;94m888[0m  [0;1;34;94mX88K[0m   
[0;1;30;90mY88b.[0m   [0;1;30;90mY88..88P[0;1;34;94m888[0m [0;1;34;94md88PY88..88P[0;34m.d8""8b.[0m 
 [0;1;30;90m"Y8888P[0m [0;1;34;94m"Y88P"[0m [0;1;34;94m88888P"[0m  [0;34m"Y88P"[0m [0;34m888[0m  [0;34m888[0m 
`)
}

module.exports = {
  getInput,
  printSpaces,
  onSuccess,
  onError,
  printErrors,
  readableBytes,
  printBanner
}

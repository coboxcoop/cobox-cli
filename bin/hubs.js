exports.command = "hubs <command>"
exports.desc = "register, connect with and send commands to your hub device(s)"
exports.builder = function (yargs) {
  return yargs
    .commandDir('hubs')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}

const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')

exports.command = 'down [options]'
exports.desc = 'close a space or all spaces'
exports.builder = pick(options, ['name', 'address'])
exports.handler = down

async function down (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address

  try {
    let spaces = await client.get('spaces')

    if (!name && !address) {
      await Promise.all([
        client.delete(['spaces', 'connections']),
        client.delete(['spaces', 'mounts'])
      ])
      process.stdout.write(JSON.stringify(spaces))
    } else {
      let space = spaces.find((space) => space.name === name || space.address === address)
      if (space) return await onceSpace(space)
      else return console.log(`! ${chalk.bold(`space doesn't exist`)}`)
    }
  } catch (err) {
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }

  async function onceSpace (space) {
    await Promise.all([
      client.delete(['spaces', space.address, 'mounts'], {}, space),
      client.delete(['spaces', space.address, 'connections'], {}, space)
    ])
    process.stdout.write(JSON.stringify(space))
  }
}

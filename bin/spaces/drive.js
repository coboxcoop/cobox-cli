exports.command = "drive <command>"
exports.desc = "interact with your space's drive"
exports.builder = function (yargs) {
  return yargs.commandDir('drive')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}

const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../../lib/options')

exports.command = 'ls [options]'
exports.desc = 'list files in a drive directory'
exports.builder = pick(options, ['name', 'address', 'dir'])
exports.handler = ls

async function ls (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address,
    dir = argv.dir

  try {
    let history = await client.get(['spaces', name || address, 'drive', 'readdir'], {}, { dir })
    process.stdout.write(JSON.stringify(history))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}

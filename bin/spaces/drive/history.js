const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../../lib/options')

exports.command = 'history [options]'
exports.desc = 'get drive history for a given space'
exports.builder = pick(options, ['name', 'address'])
exports.handler = history

async function history (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address

  try {
    let history = await client.get(['spaces', name || address, 'drive', 'history'])
    process.stdout.write(JSON.stringify(history))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}

const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const path = require('path')
const options = require('../lib/options')

exports.command = 'invite <name|address> <public-key>'
exports.desc = 'invite a peer to a space'
exports.builder = {}
exports.handler = async function (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const publicKey = argv.publicKey,
    name = argv.name,
    address = argv.address

  try {
    var spaces = await client.get('spaces')
    var space = spaces.find((space) => space.name === name || space.address === address)
    if (space) return await onceSpace()
    else return console.log(`! ${chalk.bold(`space doesn't exist`)}`)
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }

  async function onceSpace () {
    let data = await client.post(['spaces', space.address, 'invites'], {}, { publicKey })
    if (data.response) return console.error(data.response.data.errors)
    process.stdout.write(JSON.stringify(data.content))
  }
}


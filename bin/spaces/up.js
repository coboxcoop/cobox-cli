const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')

exports.command = 'up [options]'
exports.desc = 'open a space or all spaces'
exports.builder = pick(options, ['name', 'code'])
exports.handler = up

async function up (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    code = argv.code

  try {
    let spaces = await client.get('spaces')

    if (code) {
      let space = await client.post('spaces', {}, { code })
      await onceSpace(space)
    } else if (!name) {
      if (!spaces.length) return console.log(`! ${chalk.bold(`you have no spaces`)} create one with\n${chalk.hex('#ACFFAC')(`spaces up --name <name>`)}`)
      // TODO: handle already mounted or connected spaces
      // TODO: handle mountAll in API
      await Promise.all([
        client.post(['spaces', 'connections']),
        client.post(['spaces', 'mounts'])
      ])
      process.stdout.write(JSON.stringify(spaces))
    } else {
      let space = spaces.find((space) => space.name === name)
      if (space) return await onceSpace(space)
      space = await client.post('spaces', {}, { name })
      await onceSpace(space)
    }
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }

  async function onceSpace (space) {
    await Promise.all([
      client.post(['spaces', space.address, 'mounts'], {}, space),
      client.post(['spaces', space.address, 'connections'], {}, space)
    ])
    process.stdout.write(JSON.stringify(space))
  }
}

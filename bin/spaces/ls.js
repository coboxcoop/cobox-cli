const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')

exports.command = 'ls'
exports.desc = 'view your spaces'
exports.handler = ls

async function ls (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    let spaces = await client.get('spaces')
    process.stdout.write(JSON.stringify(spaces))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}

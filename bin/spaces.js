exports.command = 'spaces <command>'
exports.desc = 'view and interact with your spaces'
exports.builder = function (yargs) {
  return yargs
    .commandDir('spaces')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}

const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const chalk = require('chalk')
const options = require('../lib/options')

exports.command = 'ls'
exports.desc = 'list your replicators'
exports.builder = {}
exports.handler = ls

async function ls (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    let replicators = await client.get('replicators')
    process.stdout.write(JSON.stringify(replicators))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}

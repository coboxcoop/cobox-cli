const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')

exports.command = 'up [options]'
exports.desc = 'open a backup or all backups'
exports.builder = pick(options, ['name', 'address'])
exports.handler = up

async function up (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address

  try {
    let replicators = await client.get('replicators')

    if (!(name || address)) {
      if (!replicators.length) return console.log(`! ${chalk.bold(`you have no replicators`)} create one with\n${chalk.hex('#ACFFAC')(`cobox replicators up --name <name> --address <address>`)}`)
      await client.post(['replicators', 'connections'])
      process.stdout.write(JSON.stringify(replicators))
    } else {
      let replicator = replicators.find((replicator) => replicator.name === name || replicator.address === address)
      if (replicator) return await onceReplicator(replicator)
      if (!address) return console.log(`! ${chalk.bold(`you must provide an address`)}`)
      replicator = await client.post('replicators', {}, { name, address })
      await onceReplicator(replicator)
    }
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }

  async function onceReplicator (replicator) {
    await client.post(['replicators', replicator.address, 'connections'], { id: replicator.address }, replicator)
    process.stdout.write(JSON.stringify(replicator))
  }
}

const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')

exports.command = 'down [options]'
exports.desc = 'close a backup or all backups'
exports.builder = pick(options, ['name', 'address'])
exports.handler = down

async function down (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address

  try {
    if (!name && !address) {
      var connections = await client.delete(['replicators', 'connections'])
      process.stdout.write(JSON.stringify(connections))
    } else {
      let replicators = await client.get('replicators')
      let replicator = replicators.find((replicator) => replicator.name === name || replicator.address === address)
      if (replicator) return await onceReplicator(replicator)
      else return console.log(`! ${chalk.bold(`replicator doesn't exist`)}`)
    }
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }

  async function onceReplicator (replicator) {
    await client.delete(['replicators', replicator.address, 'connections'], { id: replicator.address }, replicator)
    process.stdout.write(JSON.stringify(replicator))
  }
}

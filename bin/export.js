const Client = require('cobox-client')
const pick = require('lodash.pick')
const { buildBaseURL } = require('cobox-client/util')
const chalk = require('chalk')
const options = require('./lib/options')

exports.command = 'export [options]'
exports.desc = 'export your keys'
exports.builder = pick(options, ['dir'])
exports.handler = async function (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const dir = argv.dir

  try {
    const data = await client.put(['keys', 'export'], {}, { dir })
    process.stdout.write(JSON.stringify(data))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}

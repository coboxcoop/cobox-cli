exports.command = "backups <command>"
exports.desc = "tell a device to backup a space"
exports.builder = function (yargs) {
  return yargs
    .commandDir('backups')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}

const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const chalk = require('chalk')
const options = require('../lib/options')

exports.command = 'ls'
exports.desc = 'view your registered cobox devices'
exports.handler = ls

async function ls (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    let devices = await client.get(['admin', 'devices'])
    process.stdout.write(JSON.stringify(devices))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}

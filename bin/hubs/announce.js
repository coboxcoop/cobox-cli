const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')

exports.command = 'announce <name|address>'
exports.desc = 'announce authentication details over the LAN'
exports.builder = {}
exports.handler = start

async function start (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name

  try {
    let device = await client.get(['admin', 'devices', name || address])
    let command = await client.post(['admin', 'devices', device.address, 'commands', 'announce'])
    process.stdout.write(JSON.stringify(command))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}

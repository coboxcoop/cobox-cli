const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const path = require('path')
const options = require('../lib/options')

exports.command = 'invite <name|address> <pubkey>'
exports.desc = 'invite a peer to become an admin for a cobox device'
exports.builder = {}
exports.handler = async function (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const publicKey = argv.publicKey,
    name = argv.name,
    address = argv.address

  try {
    var devices = await client.get(['admin', 'devices'])
    var device = devices.find((device) => device.name === name || device.address === address)
    if (device) return await onceSpace()
    else return console.log(`! ${chalk.bold(`device doesn't exist`)}`)
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }

  async function onceSpace () {
    let data = await client.post(['admin', 'devices', device.address, 'invites'], {}, { publicKey })
    process.stdout.write(data)
  }
}


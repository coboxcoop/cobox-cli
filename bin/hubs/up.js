const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')

exports.command = 'up [options]'
exports.desc = 'connect to a cobox device or all registered devices'
exports.builder = pick(options, ['name', 'code', 'address', 'key'])
exports.handler = up

async function up (argv) {
  const opts = merge(argv, process.stdin)
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = opts.name,
    address = opts.address,
    encryptionKey = opts.key
    code = opts.code

  try {
    let devices = await client.get(['admin', 'devices'])

    if (code) {
      let device = await client.post(['admin', 'devices'], {}, { code })
      await onceDevice(device)
    } else if (name || address || encryptionKey) {
      if (!(name && address && encryptionKey)) return console.log(`! ${chalk.bold(`name, address and key required`)}, use the following command:\n${chalk.hex('#ACFFAC')(`devices up --name <name> --address <address> --key <key>`)}`)
      if (!devices.length) return console.log(`! ${chalk.bold(`you have no registered cobox devices`)}, join one with\n${chalk.hex('#ACFFAC')(`devices up --code <code> or devices up --name <name> --address <address> --key <key>`)}`)
      await client.post(['admin', 'devices', 'connections'])
      process.stdout.write(JSON.stringify(devices))
    } else {
      let device = devices.find((device) => device.name === name || device.address === address)
      if (device) return await onceDevice(device)
      device = await client.post(['admin', 'devices'], {}, { name, address, encryptionKey })
      await onceDevice(device)
    }
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }

  async function onceDevice (device) {
    let data = await client.post(['admin', 'devices', device.address, 'connections'], {}, device)
    process.stdout.write(JSON.stringify(data))
  }
}

function merge (argv, pipedArgs) {
  // TODO: merge argv && process.stdin
  return argv
}

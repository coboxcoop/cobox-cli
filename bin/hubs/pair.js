const WebSocket = require('ws')
const pick = require('lodash.pick')
const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const { EventEmitter } = require('events')
const options = require('../lib/options')
const chalk = require('chalk')
const { getInput } =  require('../../util')

const DEVICE_CONNECTED = 'DEVICE_CONNECTED'

exports.command = 'pair [options]'
exports.describe = 'pair devices announcing on the LAN'
exports.builder = pick(options, ['port', 'timeout'])
exports.handler = pair

function log (str) {
  console.log()
  console.log(str)
}

function accent (str) {
  return chalk.hex('#ACFFAC')(str)
}

function pair (argv) {
  const cache = {}
  const emitter = new EventEmitter()
  const ws = new WebSocket(`ws://localhost:${argv.port}/api/devices`)
  const client = new Client({ endpoint: buildBaseURL(argv) })

  let devices,
    timeout,
    interval,
    canExit = true,
    output = []

  emitter.on('device', ({ address, id }) => {
    canExit = false
    log(`local hub device found:\npublic key: ${accent(id)}\naddress: ${accent(address)}`)

    getInput('\ngive a name to this device:\n', (name) => {
      client
        .post(['admin', 'devices'], {}, { publicKey: id, name })
        .catch(onerror)
        .then((device) => {
          log(`successfully registered as an admin for ${accent(name)}`)

          client
            .post(['admin', 'devices', device.address, 'connections'], {}, device)
            .catch(onerror)
            .then((data) => {
              log(`connected to ${(device.name)} at ${accent(data.address)}\n\nyou can now send ${accent(device.name)} commands`)

              canExit = true
              output.push(device)
              timeout = wait()
            })
        })
    })
  })

  client.get(['admin', 'devices']).catch(onerror).then((ds) => {
    devices = ds.reduce((acc, d) => { acc[d.address] = d; return acc }, {})
    ws.on('error', onerror)
    ws.on('message', onmessage)
    timeout = wait()
    interval = dotdotdot()
  })

  function onmessage (m) {
    var msg = JSON.parse(m.toString())
    var { author: id, timestamp, address } = msg.data
    if (devices[address] || cache[id]) return
    timeout = wait()
    if (msg.type === DEVICE_CONNECTED) emitter.emit('device', { address, id })
    cache[id] = timestamp
  }

  function onerror (err) {
    if (!err.response) return console.error(err)
    else console.log(err.response.data)
  }

  function dotdotdot () {
    log('searching for seeders on the LAN')
    return setInterval(() => {
      if (canExit) process.stdout.write('.')
    }, 500)
  }

  function wait () {
    if (timeout) clearTimeout(timeout)

    return setTimeout(() => {
      if (!canExit) return
      ws.close()
      if (interval) clearInterval(interval)
      if (output.length) {
        console.log()
        return process.stdout.write(JSON.stringify(output))
      }
      log(`no new hub devices found`)
    }, argv.timeout)
  }
}

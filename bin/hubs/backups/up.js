const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../../lib/options')

exports.command = 'up <device> [options]'
exports.desc = 'tell a cobox to start backing up a space'
exports.builder = pick(options, ['name', 'address', 'device'])
exports.handler = start

async function start (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const device = argv.device,
    name = argv.name,
    address = argv.address

  try {
    if (!name && !address) return console.log(`! ${chalk.bold(`you must provide a name and address`)}`)
    let d = await client.get(['admin', 'devices', device])
    let command = await client.post(['admin', 'devices', d.address, 'commands', 'replicate'], {}, { name, address })
    process.stdout.write(JSON.stringify(command))
  } catch (err) {
    console.error(err)
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}

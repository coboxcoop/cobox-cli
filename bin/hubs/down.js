const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')

exports.command = 'down [options]'
exports.desc = 'disconnect to a cobox device or all registered devices'
exports.builder = pick(options, ['name', 'address'])
exports.handler = down

async function down (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address

  try {
    let devices = await client.get(['admin', 'devices'])

    if (!name && !address) {
      await client.delete(['admin', 'devices', 'connections'])
      process.stdout.write(JSON.stringify(devices))
    } else {
      let device = devices.find((device) => device.name === name || device.address === address)
      if (device) return await onceDevice(device)
      else return console.log(`! ${chalk.bold(`device doesn't exist`)}`)
    }
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }

  async function onceDevice (device) {
    let data = await client.delete(['admin', 'devices', device.address, 'connections'], {}, device)
    process.stdout.write(JSON.stringify(data))
  }
}

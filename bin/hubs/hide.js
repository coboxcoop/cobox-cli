const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')

exports.command = 'hide <name|address>'
exports.desc = "tell an announcing device to hide"
exports.builder = pick(options, ['name', 'address'])
exports.handler = start

async function start (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address

  try {
    let device = await client.get(['admin', 'devices', name || address])
    let command = await client.post(['admin', 'devices', device.address, 'commands', 'hide'])
    process.stdout.write(JSON.stringify(command))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}

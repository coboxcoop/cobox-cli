const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const chalk = require('chalk')

exports.command = 'nickname <name>'
exports.desc = 'change your public name'
exports.builder = {}
exports.handler = async function (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name

  try {
    var data = await client.patch('profile', {}, { name })
    process.stdout.write(JSON.stringify(data))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}

module.exports = {
  address: {
    description: 'address of the space',
    type: 'string',
    alias: 'a',
  },
  name: {
    description: 'name of the space',
    type: 'string',
    alias: 'n'
  },
  key: {
    description: 'encryption key of the space',
    type: 'string',
    alias: 'k',
  },
  publicKey: {
    description: 'public key of a peer',
    type: 'string',
    alias: 'p',
  },
  code: {
    description: 'an invite code',
    type: 'string',
    alias: 'c',
  },
  device: {
    description: 'name of the registered device',
    type: 'string',
    alias: 'd'
  },
  dir: {
    description: 'specify a path',
    type: 'string'
  },
  port: {
    alias: 'p',
    number: true,
    describe: 'cobox server port',
    default: 9112
  },
  timeout: {
    alias: 't',
    number: true,
    describe: 'set a timeout',
    default: 3000
  }
}
